package com.apirest.controllers;

import com.apirest.models.Tarjeta;
import com.apirest.services.TarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class TarjetaController {

    @Autowired
    private TarjetaService tarjetaService;

    @GetMapping("")
    public String root(){
        return "Practioner Backend - Entregable 01";
    }

    @GetMapping("/tarjetas")
    public List<Tarjeta> getTarjetas(){ return tarjetaService.getTarjetas();}

    @GetMapping("/tarjetas/size")
    public int countTarjetas(){ return tarjetaService.getTarjetas().size();}

    @GetMapping("/tarjetas/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        Tarjeta tr =tarjetaService.getTarjeta(id);
        if(tr == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(tr);
    }

    @PostMapping("/tarjetas")
    public ResponseEntity<String> addTarjetas(@RequestBody Tarjeta tarjeta) {
        tarjetaService.addTarjeta(tarjeta);
        return new ResponseEntity<>("Tarjeta creada con éxito!", HttpStatus.CREATED);
    }

    @PutMapping("/tarjetas/{id}")
    public ResponseEntity updateTarjeta(@PathVariable int id,
                                         @RequestBody Tarjeta tarjetaupdate) {
        Tarjeta tr = tarjetaService.getTarjeta(id);
        if (tr == null) {
            return new ResponseEntity<>("Tarjeta no identificada.", HttpStatus.NOT_FOUND);
        }
        tarjetaService.updateTarjeta(id-1, tarjetaupdate);
        return new ResponseEntity<>("Tarjeta actualizada correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/tarjetas/{id}")
    public ResponseEntity deleteTarjeta(@PathVariable Integer id) {
        Tarjeta tr = tarjetaService.getTarjeta(id);
        if(tr == null) {
            return new ResponseEntity<>("Tarjeta no encontrado.", HttpStatus.NOT_FOUND);
        }
        tarjetaService.removeTarjeta(id - 1);
        return new ResponseEntity<>("Tarjeta eliminada!.", HttpStatus.OK);
    }

    @PatchMapping("/tipo-tarjetas/{id}")
    public ResponseEntity patchTarjetas(@RequestBody Tarjeta tarjetaDescrip,
                                              @PathVariable int id){
        Tarjeta tr = tarjetaService.getTarjeta(id);
        if (tr == null) {
            return new ResponseEntity<>("Tarjeta no encontrado.", HttpStatus.NOT_FOUND);
        }
        tr.setDescripcion(tarjetaDescrip.getDescripcion());
        tarjetaService.updateTarjeta(id-1, tr);
        return new ResponseEntity<>(tr, HttpStatus.OK);
    }
}
