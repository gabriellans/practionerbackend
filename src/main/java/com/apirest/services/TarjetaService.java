package com.apirest.services;

import com.apirest.models.Cliente;
import com.apirest.models.Tarjeta;
import com.apirest.models.TipoTarjeta;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TarjetaService {

    private List<Tarjeta> dataList = new ArrayList<>();

    public TarjetaService(){
        List<TipoTarjeta> dataTipoTarjeta = new ArrayList<>();
        dataTipoTarjeta.add(new TipoTarjeta(1,"Tarjeta de Crédito"));
        dataTipoTarjeta.add(new TipoTarjeta(2,"Tarjeta de Débito"));
        dataTipoTarjeta.add(new TipoTarjeta(3,"BFREE"));

        List<Cliente> dataCliente = new ArrayList<>();
        dataCliente.add(new Cliente(1,"Nicolas Hilario"));
        dataCliente.add(new Cliente(2,"Jimmy Hilario"));
        dataCliente.add(new Cliente(3,"Vasco Hilario"));
        dataCliente.add(new Cliente(4,"Isabella Hilario"));

        dataList.add(new Tarjeta(1, "Tarjeta Personal", 123456, dataCliente.get(0),dataTipoTarjeta.get(0)));
        dataList.add(new Tarjeta(2, "Tarjeta de Trabajo ", 987654, dataCliente.get(1),dataTipoTarjeta.get(1)));
        dataList.add(new Tarjeta(3, "Tarjeta de Alimentos ", 7477441, dataCliente.get(2),dataTipoTarjeta.get(2)));

    }

    public List<Tarjeta> getTarjetas() {
        return dataList;
    }

    public Tarjeta getTarjeta(long index) throws IndexOutOfBoundsException {
        if(getIndex(index)>=0) {
            return dataList.get(getIndex(index));
        }
        return null;
    }

    public Tarjeta addTarjeta(Tarjeta newTarjeta){
        dataList.add(newTarjeta);
        return newTarjeta;
    }

    public Tarjeta updateTarjeta(int index, Tarjeta newTarjeta) throws IndexOutOfBoundsException{
        dataList.set(index,newTarjeta);
        return dataList.get(index);
   }

   public Tarjeta removeTarjeta(int index) throws IndexOutOfBoundsException{
        int pos = dataList.indexOf(dataList.get(index));
        return dataList.remove(pos);
   }

    public int getIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<dataList.size()) {
            if(dataList.get(i).getId() == index){
                return(i); }
            i++;
        }
        return -1;
    }
}
