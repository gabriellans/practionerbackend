package com.apirest.models;

public class Cliente {

    private long id;
    private String nombre;

    public Cliente(){}

    public Cliente(long id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }

    public long getId(){
        return this.id;
    }

    public String nombre(){
        return this.nombre;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
