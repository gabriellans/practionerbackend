package com.apirest.models;

public class TipoTarjeta {
    private long id;
    private String descripcion;

    public TipoTarjeta(){}

    public TipoTarjeta(long id, String descripcion){
        this.id = id;
        this.descripcion = descripcion;
    }

    public long getID(){
        return this.id;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
